const tabs = [...document.querySelectorAll(".tabs > li")];

const content = [...document.querySelectorAll(".tabs-content > li")];

const handleChangeContent = (idx) => {
  content.forEach((current) => current.classList.remove("active"));

  content[idx].classList.add("active");
};

const handleChangeTab = (tab) => {
  const currentTab = tab.target;

  const index = tabs.indexOf(currentTab);

  tabs.forEach((current) => current.classList.remove("active"));

  currentTab.classList.add("active");

  handleChangeContent(index);
};

tabs.forEach((tab) => tab.addEventListener("click", handleChangeTab));
